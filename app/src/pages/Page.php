<?php

namespace {

	use SilverStripe\CMS\Model\SiteTree;
	use SilverStripe\Forms\GridField\GridFieldConfig;
	use SilverStripe\Forms\GridField\GridFieldToolbarHeader;
	use SilverStripe\Forms\GridField\GridFieldAddNewButton;
	use SilverStripe\Forms\GridField\GridFieldSortableHeader;
	use SilverStripe\Forms\GridField\GridFieldDataColumns;
	use SilverStripe\Forms\GridField\GridFieldPaginator;
	use SilverStripe\Forms\GridField\GridFieldEditButton;
	use SilverStripe\Forms\GridField\GridFieldDeleteAction;
	use SilverStripe\Forms\GridField\GridFieldDetailForm;
	use Symbiote\GridFieldExtensions\GridFieldTitleHeader;
	use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

	class Page extends SiteTree {

		public static function get_gridfieldconfig($SortFieldName = 'SortOrder') {
			$gridFieldConfig = GridFieldConfig::create()->addComponents(
				new GridFieldToolbarHeader(),
				new GridFieldAddNewButton('toolbar-header-right'),
				$SortFieldName && class_exists(GridFieldTitleHeader::class) ? new GridFieldTitleHeader(
				) : new GridFieldSortableHeader(),
				new GridFieldDataColumns(),
				new GridFieldPaginator(20),
				new GridFieldEditButton(),
				new GridFieldDeleteAction(),
				new GridFieldDetailForm()
			);
			if ($SortFieldName) {
				if (class_exists(GridFieldOrderableRows::class)) {
					$gridFieldConfig->addComponent(new GridFieldOrderableRows($SortFieldName));
				} elseif (class_exists('GridFieldSortableRows')) {
					$gridFieldConfig->addComponent(new GridFieldSortableRows($SortFieldName));
				}
			}
			return $gridFieldConfig;
		}

		public function TemplateClassName() {
			$reflect = new ReflectionClass($this);
			return $reflect->getShortName();
		}

	}
}
