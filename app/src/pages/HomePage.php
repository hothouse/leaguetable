<?php

use SilverStripe\Assets\Image;
use SilverStripe\Assets\FileNameFilter;
use Bummzack\SortableFile\Forms\SortableUploadField;

class HomePage extends Page {

	private static $many_many = array(
		'HomePageImages' => Image::class
	);

	private static $many_many_extraFields = array(
		'HomePageImages' => array('SortOrder' => 'Int')
	);

	private static $owns = array(
		'HomePageImages'
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->removeByName('HomePageImages');
		$filter = FileNameFilter::create();
		$FolderName = $filter->filter($this->Title);
		$fields->addFieldToTab(
			'Root.Images',
			SortableUploadField::create('HomePageImages')
				->setFolderName($FolderName)
				->setAllowedExtensions(array('jpg', 'jpeg', 'png'))
		);
		return $fields;
	}

	public function Image() {
		return $this->Images()->First();
	}

	public function Images() {
		return $this->HomePageImages()->sort('SortOrder');
	}

}
