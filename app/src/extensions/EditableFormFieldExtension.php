<?php

use SilverStripe\UserForms\FormField\UserFormsStepField;
use SilverStripe\UserForms\FormField\UserFormsGroupField;
use SilverStripe\Forms\CompositeField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\ORM\DataExtension;

class EditableFormFieldExtension extends DataExtension {

	public function afterUpdateFormField($field) {
		$ClassName = get_class($field);
		if (!in_array(
			$ClassName,
			array(UserFormsStepField::class, UserFormsGroupField::class, CompositeField::class, CheckboxField::class)
		)) {
			$field->addExtraClass('form-control');
		}
	}

}