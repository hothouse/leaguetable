<?php

use SilverStripe\Control\Director;
use SilverStripe\Core\Environment;
use SilverStripe\i18n\i18n;

// db
if(!Environment::getEnv('SS_DATABASE_NAME')) {
	global $database;
	$database = 'leaguetable';
}

// i18n
i18n::set_locale('en_NZ');

// Director rules
if(!Director::is_cli()) {
	//Director::forceWWW();
	if (Director::isLive()) {
		//Director::forceSSL();
	}
}
