<div class="element_content__content <% if $Style %>element_content__$ExtraClass<% end_if %>">
    <% if $ShowTitle %><h3>$Title</h3><% end_if %>
    <% if $Content %>$Content<% end_if %>
    <% if $Panels %>
		<div id="accordion-{$ID}" class="accordion" role="tablist">
            <% loop $Panels %>
				<div class="card">
					<div class="card-header" id="headingOne">
						<h3 class="mb-0">
							<button class="btn btn-link p-0" data-toggle="collapse" data-target="#collapse-{$ID}" aria-expanded="true" aria-controls="collapse-{$ID}">
								<h3 class="m-0">$Title</h3>
							</button>
						</h3>
					</div>

					<div id="collapse-{$ID}" class="collapse <% if $First %>show<% end_if%>" aria-labelledby="headingOne" data-parent="#accordion">
						<div class="card-body">
                            <% if $Image %>
								<img src="$Image.URL" class="img-responsive" alt="$Title.ATT">
                            <% end_if %>
                            $Content
                            <% if $ElementLink %>$ElementLink<% end_if %>
						</div>
					</div>
				</div>
            <% end_loop %>
		</div>
    <% end_if %>
</div>