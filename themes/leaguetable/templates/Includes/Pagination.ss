<% with $Results %>
	<% if $MoreThanOnePage %>
		<ul class="pagination pagination-centered">
			<li<% if not $NotFirstPage %> class="disabled"<% end_if %>>
				<a href="$PrevLink">&laquo;</a>
			</li>
			<% loop $PaginationSummary(4) %>
				<li <% if $CurrentBool %> class="active"<% else_if not $Link %> class="disabled"<% end_if %>>
					<% if $Link %>
						<a href="$Link">$PageNum</a>
					<% else %>
						<a href="#">...</a>
					<% end_if %>
				</li>
			<% end_loop %>
			<li<% if not $NotLastPage %> class="disabled"<% end_if %>>
				<a href="$NextLink">&raquo;</a>
			</li>
		</ul>
	<% end_if %>
<% end_with %>
